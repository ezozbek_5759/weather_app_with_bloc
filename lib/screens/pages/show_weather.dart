import 'package:flutter/material.dart';
import 'package:weather_app_with_bloc/models/weather_model.dart';

class ShowWeather extends StatelessWidget {
  WeatherModel weather;
  final city;
  ShowWeather(this.weather, this.city);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 32, right: 32, top: 10),
      child: Column(
        children: [
          Text(
            city,
            style: const TextStyle(fontSize: 30, color: Colors.white),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            weather.getTemp.round().toString() + "C",
            style: const TextStyle(color: Colors.white, fontSize: 50),
          ),
          const Text(
            "Temprature",
            style: TextStyle(fontSize: 14, color: Colors.white),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Text(
                    weather.getMinTemp.round().toString() + "C",
                    style: const TextStyle(fontSize: 14, color: Colors.white),
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                    weather.getMaxTemp.round().toString() + "C",
                    style: const TextStyle(fontSize: 14, color: Colors.white),
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
