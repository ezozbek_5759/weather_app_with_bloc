import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:weather_app_with_bloc/models/weather_model.dart';

class WeatherRepository {
  Future<WeatherModel> getWeather(String city) async {
    final result = await http.Client().get(Uri(
        host:
            "https://api.openweathermap.org/data/2.5/weather?q=$city&appid=894bdead74cca565995c8b8c9b5ace79"));

    if (result.statusCode != 200) throw Exception();
    return parsedJson(result.body);
  }

  WeatherModel parsedJson(final response) {
    final jsonDecoded = json.decode(response);

    final jsonWeather = jsonDecoded["main"];

    return WeatherModel.fromJson(jsonWeather);
  }
}
