import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:weather_app_with_bloc/models/weather_model.dart';
import 'package:weather_app_with_bloc/repository/weather_repository.dart';

part 'weather_event.dart';
part 'weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  WeatherRepository weatherRepository;
  WeatherBloc(this.weatherRepository) : super(WeatherInitial()) {
    on<FetchWeather>((event, emit) async {
      if (event is FetchWeather) {
        emit(WeatherIsLoading());
        try {
          WeatherModel weather =
              await weatherRepository.getWeather(event._city);
          WeatherIsLoaded(weather);
        } catch (_) {
          WeatherIsNotLoaded();
        }
      }
    });
    on<ResetWeather>((event, emit) async {
      if (event is ResetWeather) {
        emit(WeatherIsNotSearched());
      }
    });
  }
}
